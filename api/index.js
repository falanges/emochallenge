import { ApolloServer, UserInputError, gql } from 'apollo-server';
import { v1 as uuid } from 'uuid';
const persons = [
    {
        name: "Michael",
        phone: "030-3456",
        street: "Architect street",
        city: "Baltimore",
        id: "3d595650-3436-11e9-bc57-8b80ba54c431"
    },
    {
        name: "Yeison",
        phone: "030-3457",
        street: "JSON street",
        city: "Pereyra",
        id: "3d595650-3436-11e9-bc57-8b80ba54c431"
    },
    {
        name: "Santiago",
        street: "DevOps++ street",
        city: "Medellin",
        id: "3d595650-3436-11e9-bc57-8b80ba54c431"
    },
    {
        name: "Matias",
        street: "Printer Fixer street",
        city: "Ushuaia",
        id: "3d595650-3436-11e9-bc57-8b80ba54c431"
    }
];

const typeDefinitions = gql`
    type Address {
        street: String!
        city: String!
    }    

    type Person {
        name: String!
        phone: String
        address: Address!
        id: ID!
    }
    type Query {
        personCount: Int!
        allPersons: [Person]!
        findPerson(name:String!): Person
    }
    type Mutation {
        addPerson(
            name: String!
            phone: String
            street: String!
            city: String!
        ): Person
    }
`

const resolvers = {
    Query: {
        personCount: () => persons.length,
        allPersons: () => persons,
        findPerson: (root, args) => {
            const {name} = args
            return persons.find(person => person.name === name)
        }
    },
    Mutation: {
        addPerson: (root, args) => {
            if (persons.find(p => p.name === args.name)) {
                throw new UserInputError('Name must be unique', {
                    invalidArgs: args.name
                })
            }
            // const {name, phone, street, city} = args
            const person = { ...args, id: uuid()}
            persons.push(person) // update db with new person
            return person
        }
    },
    Person: {
        address: (root) => {
            return {
                street: root.street,
                city: root.city
            }
        }
    }
}
const server = new ApolloServer({
    typeDefs: typeDefinitions,
    resolvers
})

server.listen().then(({ url }) => {
    console.log(`Server so ready for you at ${url}`);
})